FROM python:3.10-slim
WORKDIR /app
COPY *.py /app
COPY --link outputs /app
COPY --link input_data /app
COPY --link test /app
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python"]
