#!/bin/bash
oarsub -l /nodes=1,walltime=15:00:00 --project pr-airsea-modeling /home/clemensi/job_LU.sh
oarsub -l /nodes=1,walltime=15:00:00 --project pr-airsea-modeling /home/clemensi/job_det.sh
oarsub -l /nodes=1,walltime=15:00:00 --project pr-airsea-modeling /home/clemensi/job_PS.sh
oarsub -l /nodes=1,walltime=15:00:00 --project pr-airsea-modeling /home/clemensi/job_PP.sh
oarsub -l /nodes=1,walltime=15:00:00 --project pr-airsea-modeling /home/clemensi/job_Without_compensation.sh
