# Stochastic Grid Perturbation on QGM

Stochastic Grid Perturbation applied to a PyTorch implementation of multi-layer quasi-geostrophic model on rectangular domain with solid boundaries, with parameterizatition described in the paper (Modified hyper-viscosity for coarse-resolution ocean models)[https://arxiv.org/abs/2204.13914].

## Requirements

```
python==3.10
torch==2.3
numpy==1.26
scipy==1.13
matplotlib==3.9
geomloss==0.2
```

Tested with Intel Xeon CPUs with 64Go of RAM.

There is a Dockerfile for Docker users. It can be used as follows:
```
docker build -r sgp-app .
docker run sgp-app main.py det # for instance to launch the deterministic simulation
docker run sgp-app exploit_results.py fig_p # plot corresponding figure
```

## Usage

The entry point of the code is `main.py` and can be ran with

```
# The following commands take dozen of hours each to run;
# the output data is available for download (see Data availability section below)
python3 main.py det
python3 main.py det_HR
python3 main.py LU_enstrophy
python3 main.py Perturbed_steps
python3 main.py Perturbed_points
python3 main.py Without_compensation
python3 main.py Perturbed_steps_each_gyre # additional experiment
```

Model parameters are defined in a python dictionary as follows

```
    param = {
        'nx': 97, # number of p-points in x direction
        'ny': 121, # number of p-points in y direction
        'Lx': 96*40e3, # Length in x direction (m)
        'Ly': 120*40e3, # Length in y direction (m)
        'nl': 3, # number of layers
        'H': [350., 750., 2900.], # layer thickness (m)
        'g_prime': [0.025, 0.0125], # reduced gravities (m/s^2)
        'f0':  9.37456e-5, # Coriolis param. (s^-1)
        'beta': 1.7536e-11, # Coriolis gradient (m^-1/s)
        'delta_ek': 2.0, # bottom Ekman layer thickness (m)
        'tau0': 2.0e-5, # wind stress magnitude (m/s^2)
        'dt': 7200., # timestep (s)
        'a2': 0., # laplacian diffusion coef (m^2/s)
        'a4': 5e11, # bi-harmonic visc. coef. (m^4/s)
        'alpha_bc': 0.2, # boundary condition coef. (non-dim.)
        'rand_model': RAND_MODEL[experiment], # '', 'grid_perturbation_steps', 'grid_perturbation_points', 'energy_preserve' or 'enstrophy_preserve'
        'compensation_PG': COMPENSATION_PG[experiment], # 'continuous', 'discrete', 'q' or 'none'
        'alpha_PG': 5e-5,
        'base_noise': 'eof', # 'dwt', 'eof', 'dmd'
        'file_modes': 'input_data/eof_40km.npz', #
        'mean_drift': 'input_data/drift_40km.npz', # add mean drift term
        'n_ens': 30, # ensemble size
        'filt_width': 3, # filter width to build local fluctuations
        'wave_level': 2, # wavelet decomposition levels
        'device': 'cpu', # 'cuda' or 'cpu'
    }
```
A one-year simulation takes dozen of hours. Once the data is exported (folder outputs/),
, a coarse-graining procedure must be performed to downsample the high-resolution simulation data on the coarse grid, using the command:
```
python3 coarse_grain.py
```
After that, Figures and Tables can be generated with the following commands:
```
python3 exploit_results.py fig_visualise_perturbation
python3 exploit_results.py fig_mean_pointwise_p
python3 exploit_results.py fig_mean_pointwise_q
python3 exploit_results.py fig_std_pointwise_p
python3 comp_spec.py
# The following commands need either 64Go of RAM
# or the folders tmp_moments_u/, tmp_moments_q/ (see Data availability section below)
python3 exploit_results.py fig_moments_u # might need 64Go of RAM
python3 exploit_results.py fig_moments_q # might need 64Go of RAM
python3 exploit_results.py print_tables # might need 64Go of RAM
python3 exploit_results.py radar_chart # might need 64Go of RAM
# Additional analysis to estimate contribution of the term $trace(\sigma \nabla h)$:
python3 exploit_results.py estimate_contribution_trH
# Additional moment analysis:
python3 exploit_results.py fig_moments_q_around_jet # might need 64Go of RAM
python3 exploit_results.py fig_moments_u_around_jet # might need 64Go of RAM
```

The initial ensemble can be re-generated with the script
```
python3 initial_ensemble_creator.py
```
which launches a 15-years spinup of a very high resolution
setting (5km resolution, ~3 years/day on 8 cores)
then an additional spinup on the 40km resolution of
the 30-members ensemble of 1 year.

## Data availability
The data produced by the following commands:
```
python3 main.py det
python3 main.py det_HR
python3 main.py LU_enstrophy
python3 main.py Perturbed_steps
python3 main.py Perturbed_points
python3 main.py Without_compensation
python3 exploit_results.py radar_chart
```
is available for download on [Zenodo](https://doi.org/10.5281/zenodo.11654106). It avoids multiple days of computational ressource as well as the need for 64Go of RAM to compute the moments.
The three archives available on [Zenodo](https://doi.org/10.5281/zenodo.11654106) are to be extracted directly in the main folder.

## Acknowledgments

Parts of the computations realized with this code were performed using the [GRICAD infrastructure](https://gricad.univ-grenoble-alpes.fr), which is supported by Grenoble research communities.
