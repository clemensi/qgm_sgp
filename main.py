import sys
import torch
import QGM
import IO_QGM
from description_experiments import get_param, Experiment, EXPERIMENT_BY_NAME, FOLDERNAMES

if __name__ == "__main__":
    notice_usage = "EXPERIMENT can be " + str(list(EXPERIMENT_BY_NAME.keys()))
    if len(sys.argv) != 2:
        print("Usage: python3 main.py EXPERIMENT")
        print(notice_usage)
        exit()

    experiment_name = sys.argv[1]
    if experiment_name not in EXPERIMENT_BY_NAME:
        print("EXPERIMENT", experiment_name, "not known")
        print(notice_usage)
        exit()

    experiment = EXPERIMENT_BY_NAME[experiment_name]
    if experiment is Experiment.det_HR:
        # The fast code of L. Thiry is used for the High Resolution experiment;
        # It constrains the space steps to be constant and thus forbids SGP implementation
        # Note that some parameters are different in the high_resolution:
        # Namely, dxy, nx, ny, n_ens, a4, and dt
        import QGM_det_HR
        QGM_det_HR.main()
        exit()

    torch.backends.cudnn.deterministic = True
    nx, ny = 96+1, 120+1 
    dxy = int(40e3)
    Lx, Ly = (nx-1)*dxy, (ny-1)*dxy
    dt = 7200.
    param = get_param(experiment)
    print(param)
    qgm = QGM.QGM(param)

    # Starting input/output module and initializing qgm:
    Input_output_QGM = IO_QGM.IO_QGM(FOLDERNAMES[experiment],
            freq_save=int(1*24*3600/dt), freq_log=int(24*3600/dt),
            freq_plot=0) # save and log every day, never plot

    Input_output_QGM.initialize_pq(qgm, 'input_data/initial_ensemble.npz')
    Input_output_QGM.initialize_IO(param)

    Input_output_QGM.save_plot_log(qgm) # outputs initial state
    # Time-stepping
    n_steps = int(1 *365*24*3600/dt) + 1 # 1 year
    for _ in range(n_steps):
        qgm.step()
        Input_output_QGM.save_plot_log(qgm)
