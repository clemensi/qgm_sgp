import torch
from enum import Enum

class Experiment(Enum):
    det = 0
    LU_energy = 1 # unused
    LU_enstrophy = 2
    Perturbed_steps_u = 3 # unused
    Perturbed_steps = 4
    Perturbed_points_u = 5 # unused
    Perturbed_points = 6
    Without_compensation = 7
    Interpolated_points_q = 8 # unused
    det_HR = 9
    Perturbed_steps_each_gyre = 10

FOLDERNAMES = {Experiment.LU_energy:"outputs/energy_preservenpz/",
               Experiment.det: "outputs/det/",
               #Experiment.det_HR: "outputs/det_10km/",
               Experiment.det_HR: "outputs/det_downsampled/",
               Experiment.Perturbed_steps_u: "outputs/grid_perturbation_discretenpz/",
               Experiment.Perturbed_steps: "outputs/Perturbed_steps/",
               Experiment.Perturbed_steps_each_gyre: "outputs/Perturbed_steps_each_gyre/",
               Experiment.Perturbed_points_u: "outputs/grid_perturbation_points_discretenpz/",
               Experiment.Without_compensation: "outputs/Without_compensation/",
               Experiment.LU_enstrophy: "outputs/LU_enstrophy/",
               Experiment.Perturbed_points: "outputs/Perturbed_points/",
               Experiment.Interpolated_points_q: "outputs/grid_perturbation_points_qnpzinterpolated/",
               }
EXP_NAMES = {Experiment.LU_energy:"LU",
               Experiment.det: "Deterministic",
               Experiment.det_HR: "Deterministic HR",
               Experiment.Perturbed_steps_u: "Perturbed steps",
               Experiment.Perturbed_steps: "Perturbed steps",
               Experiment.Perturbed_steps_each_gyre: "Cumulative sum in each gyre",
               Experiment.Perturbed_points_u: r"Perturbed points",
               Experiment.Without_compensation: r"Without compensation",
               Experiment.Perturbed_points: r"Perturbed points",
               Experiment.Interpolated_points_q: r"Interpolated",
               Experiment.LU_enstrophy: "LU"}

EXPERIMENT_BY_NAME = {"det": Experiment.det,
            # "LU_energy": Experiment.LU_energy,
            "LU_enstrophy": Experiment.LU_enstrophy,
            # "Perturbed_steps_u": Experiment.Perturbed_steps_u,
            "Perturbed_steps": Experiment.Perturbed_steps,
            "Perturbed_steps_each_gyre": Experiment.Perturbed_steps_each_gyre,
            # "Perturbed_points_u": Experiment.Perturbed_points_u,
            "Perturbed_points": Experiment.Perturbed_points,
            "Without_compensation": Experiment.Without_compensation,
            # "Interpolated_points_q": Experiment.Interpolated_points_q,
            "det_HR": Experiment.det_HR,
        }

def get_param(exp: Experiment):

    if exp == Experiment.det_HR:
        return create_param(nx=385, ny=481, dxy=10000, dt=1800., n_ens=30, a4=1e10,
                rand_model='', compensation="none")
    else:
        RAND_MODEL = { Experiment.det: '',
                    Experiment.LU_energy: "energy_preserve",
                    Experiment.LU_enstrophy: "enstrophy_preserve",
                    Experiment.Perturbed_steps_u: 'grid_perturbation_steps',
                    Experiment.Perturbed_steps: 'grid_perturbation_steps',
                    Experiment.Perturbed_steps_each_gyre: 'grid_perturbation_steps',
                    Experiment.Perturbed_points_u: 'grid_perturbation_points',
                    Experiment.Perturbed_points: 'grid_perturbation_points',
                    Experiment.Without_compensation: 'grid_perturbation_points',
                    Experiment.Interpolated_points_q: 'grid_perturbation_points',
                    Experiment.det_HR: ''
                    }
        COMPENSATION_PG = { Experiment.det: 'none',
                    Experiment.LU_energy: "none",
                    Experiment.LU_enstrophy: "none",
                    Experiment.Perturbed_steps_u: 'discrete',
                    Experiment.Perturbed_steps: 'q',
                    Experiment.Perturbed_steps_each_gyre: 'q',
                    Experiment.Perturbed_points_u: 'discrete',
                    Experiment.Perturbed_points: 'q',
                    Experiment.Without_compensation: 'none',
                    Experiment.Interpolated_points_q: 'q',
                    Experiment.det_HR: 'q'
                    }
        param = create_param(nx=97, ny=121, dxy=40000, dt=7200., n_ens=30, a4=5e11,
                rand_model=RAND_MODEL[exp], compensation=COMPENSATION_PG[exp])
        if exp == Experiment.Perturbed_steps_each_gyre:
            param["special_cumulative_sum"] = True
            param["n_ens"] = 1

        return param


def create_param(nx: int=97, ny: int=121, dxy: int=40000, dt: float=7200.,
        n_ens: int=30, a4: float=5e11, rand_model: str="", compensation: str="none"):
    Lx, Ly = 3840.0e3, 4800.0e3
    assert abs(Lx-(nx-1)*dxy) < 1e-3 and abs(Ly-(ny-1)*dxy) < 1e-3
    param = {
        'n_ens': n_ens, # ensemble size
        'nx': nx, # number of p-points in x direction
        'ny': ny, # number of p-points in y direction
        'Lx': 3840.0e3, # Length in x direction (m)
        'Ly': 4800.0e3, # Length in y direction (m)
        'nl': 3, # number of layers
        'H': [350., 750., 2900.], # layer thickness (m)
        'g_prime': [0.025, 0.0125], # reduced gravities (m/s^2)
        'f0':  9.37456e-5, # Coriolis param. (s^-1)
        'beta': 1.7536e-11, # Coriolis gradient (m^-1/s)
        'delta_ek': 2.0, # bottom Ekman layer thickness (m)
        'tau0': 2.0e-5, # wind stress magnitude (m/s^2)
        'dt': dt, # timestep (s)
        'a2': 0., # laplacian diffusion coef (m^2/s)
        'a4': a4, # bi-harmonic visc. coef. (m^4/s)
        'alpha_bc': 0.2, # boundary condition coef. (non-dim.)
        'rand_model': rand_model, # '', 'grid_perturbation_steps', 'grid_perturbation_points', 'energy_preserve' or 'enstrophy_preserve'
        'compensation_PG': compensation, # 'continuous', 'discrete', 'q' or 'none'
        'alpha_PG': 5e-5,
        'base_noise': 'eof', # 'dwt', 'eof', 'dmd'
        'file_modes': 'input_data/eof_40km.npz', #
        'mean_drift': 'input_data/drift_40km.npz', # add mean drift term
        'filt_width': 3, # filter width to build local fluctuations
        'wave_level': 2, # wavelet decomposition levels
        'device': 'cuda' if torch.cuda.is_available() else 'cpu', # 'cuda' or 'cpu'
    }
    return param
