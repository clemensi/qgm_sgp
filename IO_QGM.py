"""
    This modules handles Input and Outputs related to the main module.
    this includes notably:
        - load initial ensemble
        - logs
        - save in output files
        - plot during simulation
"""
import matplotlib.pyplot as plt
import numpy as np
import torch
import os
from description_experiments import FOLDERNAMES
import torch.nn.functional as F

class IO_QGM:

    def __init__(self, output_dir: str,
            freq_save: int, freq_checknan: int=1000,
            freq_plot:int=0, freq_log: int=0):
        self.freq_checknan = freq_checknan
        self.freq_log = freq_log
        self.freq_plot = freq_plot
        self.freq_save = freq_save
        self.n_steps_save = 0
        self.output_dir = output_dir
        os.makedirs(self.output_dir) if not os.path.isdir(self.output_dir) else None
        self.t = 0.
        self.n = 0
        self.a = None

    def initialize_pq(self, qgm, initial_ensemble_file: str):
        """
            loads the ensemble file to provide pressure p and vorticity q to the qgm module
        """
        # Input restart file
        data = np.load(initial_ensemble_file)
        p = torch.from_numpy(data['p']).type(torch.float64).to(qgm.device)
        if len(p.shape) == 3: # p.shape is (nlayer, nx, ny):
            p = p.tile(qgm.n_ens,1,1,1)
        if qgm.n_ens < p.shape[0]: # more ensemble members in initialization than n_ens:
            p = p[:qgm.n_ens]
        if (p.shape[-2] == qgm.nx) and (p.shape[-1] == qgm.ny):
            qgm.p = p
        else:
            if p.shape[-1] < qgm.ny:
                # Interpolation from LR to HR
                qgm.p = F.interpolate(p, qgm.p_shape[-2:], mode='bicubic', align_corners=False)
            else:
                # Downsamping from HR to LR
                d = (p.shape[-1]-1) // (qgm.ny-1)
                x = torch.linspace(-d, d, 2*d+1, **qgm.arr_kwargs)
                x, y = torch.meshgrid(x, x, indexing='xy')
                f = torch.exp(-(x**2 + y**2) / (d**2)) / (torch.pi*d**2)
                f /= f.sum()
                p_ = F.pad(p, (d,d,d,d), mode='replicate')
                for i in range(qgm.n_ens): # loop rather vectorial calculus to avoid an impossible allocation
                    p_i = F.conv2d(p_[i].reshape((-1,)+p_.shape[-2:]).unsqueeze(1),
                            f.unsqueeze(0).unsqueeze(0)).squeeze(1).reshape(p[i].shape)[...,::d,::d]
                    qgm.p[i, :,1:-1,1:-1] = p_i[:,1:-1,1:-1] # no boundaries
                    del p_i
                del p, p_
            # Ensure mass conservation
            try:
                qgm.p = torch.einsum('lm,...mij->...lij', qgm.Cm2l, qgm.compute_dp_modes(qgm.p))
            except:
                p_modes = F.pad(torch.einsum('ml,...lij->...mij', qgm.Cl2m, qgm.p[...,1:-1,1:-1]), (1,1,1,1))
                alpha =  (qgm.alpha_matrix @ p_modes[...,:-1,:,:].mean((-2,-1)).unsqueeze(-1)).unsqueeze(-1)
                p_modes[...,:-1,:,:] += alpha * qgm.homogeneous_sol
                qgm.p = torch.einsum('lm,...mij->...lij', qgm.Cm2l, p_modes)
                del p_modes, alpha
        data.close()
        qgm.compute_q_over_f0_from_p()

    def save(self, qgm):
        """
            saves current pressure, vorticity and grid into file "pq_n.npz"
        """
        filename = os.path.join(self.output_dir, f'pq_{self.n_steps_save}.npz')
        try:
            np.savez(filename, t=self.t/(365*86400), p=qgm.p.cpu().numpy().astype('float32'), \
                    q=qgm.q_over_f0.cpu().numpy().astype('float32'),
                     dx=qgm.perturbation_dx.cpu().numpy().astype('float32')/qgm.dx_ref,
                     dy=qgm.perturbation_dy.cpu().numpy().astype('float32')/qgm.dy_ref)
        except AttributeError: # no "perturbation_dx" attribute ?
            np.savez(filename, t=self.t/(365*86400), p=qgm.p.cpu().numpy().astype('float32'), \
                    q=qgm.q_over_f0.cpu().numpy().astype('float32'))

        self.n_steps_save += 1
        if self.n % (10*self.freq_save) == 0:
            print(f'saved p to {filename}')

    def plot(self, qgm):
        n_years, n_days = int(self.t // (365*24*3600)), int(self.t % (365*24*3600) // (24*3600))
        p = (qgm.p[0,0]).cpu().numpy()
        q = (qgm.q_over_f0[0,0]).cpu().numpy()
        self.a[0].imshow(p.T, cmap='RdBu_r', origin='lower', vmin=-5, vmax=5, animated=True)
        self.a[1].imshow(q.T, cmap='RdBu_r', origin='lower', vmin=-.5, vmax=.5, animated=True)
        plt.suptitle(f't = {n_years:03d} yrs {n_days:03d} days')
        plt.tight_layout()
        plt.pause(0.5)

    def log(self, qgm):
        p, q = qgm.p.cpu().numpy(), qgm.q_over_f0.cpu().numpy()
        try:
            dx = qgm.perturbation_dx.cpu().numpy()/qgm.dx_ref
            dy = qgm.perturbation_dy.cpu().numpy()/qgm.dy_ref
            log_str = f'{self.n=:06d}, t={self.t/(365*24*60**2):.3f} yr, ' \
                      f'p: ({p.mean():+.1E}, {np.abs(p).max():.2E}) m^2/s^2, ' \
                      f'dxy: (max: {max(np.abs(dx).max(), np.abs(dy).max()):+.2E}, std: {np.std(dy):.2E}) m^2/s^2, ' \
                      f'q: ({q.mean():+.1E}, {np.abs(q).max():.2E}) f0.'
        except AttributeError:
            log_str = f'{self.n=:06d}, t={self.t/(365*24*60**2):.3f} yr, ' \
                      f'p: ({p.mean():+.1E}, {np.abs(p).max():.2E}) m^2/s^2, ' \
                      f'q: ({q.mean():+.1E}, {np.abs(q).max():.2E}) f0.'
        print(log_str)

    def initialize_IO(self, param):
        """
            creates directory, saves param file and creates figure
        """

        # Set output
        if self.freq_save > 0:
            filename = os.path.join(self.output_dir, 'param.pth')
            torch.save(param, filename)
        
        # Init. figures
        if self.freq_plot > 0:
            plt.ion()
            _, self.a = plt.subplots(1,2)
            self.a[0].set_title('Surface dynamic pressure')
            self.a[1].set_title('Surface potential vorticity')

    def save_plot_log(self, qgm):
        if self.n % self.freq_checknan == 0 and torch.isnan(qgm.p).any():
            raise ValueError('Stopping, NAN number in p at iteration {n}.')

        if self.freq_plot > 0 and self.n % self.freq_plot == 0:
            self.plot(qgm)

        if self.freq_log > 0 and self.n % self.freq_log == 0:
            self.log(qgm)
        
        if self.freq_save > 0 and self.n % self.freq_save == 0:
            self.save(qgm)

        self.t += qgm.dt
        self.n += 1
