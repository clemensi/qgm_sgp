import numpy as np

class PSD:
    """Compute Power Spectral Density (PSD)."""
    
    def __init__(self, param):
        self.nx = param['nx'] - 1 # Fourier coef defined on T-grid
        self.ny = param['ny'] - 1
        self.dx = param['Lx'] / self.nx 
        self.dy = param['Ly'] / self.ny
        self.f0 = param['f0']
        assert self.nx % 2 == 0, 'Wavenumbers length in x-axis is not even'
        assert self.ny % 2 == 0, 'Wavenumbers length in y-axis is not even'

        # Create horizontal wavenumbers
        kx = np.fft.fftfreq(self.nx, self.dx/(2*np.pi))
        ky = np.fft.fftfreq(self.ny, self.dy/(2*np.pi))
        kx, ky = np.meshgrid(kx.astype('float64'), ky.astype('float64'), indexing='ij') 
        self.kh = np.sqrt(kx**2 + ky**2).flatten()

        # Create isotropic wavenumbers
        dkx, dky = 2*np.pi/(self.nx*self.dx), 2*np.pi/(self.ny*self.dy)
        self.dkr = np.maximum(dkx,dky) # spacing
        self.nkr = np.ceil(np.sqrt(2)*np.maximum(self.nx/2,self.ny/2)).astype('int') # length
        self.kr = self.dkr*np.arange(1, self.nkr+1, dtype=np.float64)

        # Scaling factor of energy density due to DFT and integration
        self.scaling = self.dx*self.dy*np.minimum(dkx,dky)/(4*np.pi**2*self.nx*self.ny)

    def compute_spectrum(self, p):
        """Compute MKE and EKE spectrum from pressure."""
        # Geostrophic velocity on T-grid
        u = -(p[...,:,1:] - p[...,:,:-1]) / (self.f0*self.dy)
        v =  (p[...,1:,:] - p[...,:-1,:]) / (self.f0*self.dx)
        u = (u[...,1:,:] + u[...,:-1,:])/2 # interp in x
        v = (v[...,:,1:] + v[...,:,:-1])/2 # interp in y

        # MKE spectrum
        um = u.mean(axis=0)
        vm = v.mean(axis=0)
        fhat2 = (abs(np.fft.fft2(um))**2 + abs(np.fft.fft2(vm))**2)/2
        mke_spec = self.isotropic_spectrum(fhat2)
        
        # TKE spectrum
        u -= um 
        v -= vm
        fhat2 = (abs(np.fft.fft2(u))**2 + abs(np.fft.fft2(v))**2)/2
        tke_spec = self.isotropic_spectrum(fhat2).mean(axis=0)
        
        return mke_spec, tke_spec
 
    def isotropic_spectrum(self, fhat2):
        """Compute isotropic spectrum of squared 2D FFT coef"""
        assert fhat2.shape[-1] == self.ny, 'Incorrect size in y-axis'
        assert fhat2.shape[-2] == self.nx, 'Incorrect size in x-axis'
        fhat2 = fhat2.reshape((fhat2.shape[:len(fhat2.shape)-2] + (-1,)))
        fpsd = np.zeros((fhat2.shape[:len(fhat2.shape)-1] + (self.nkr,)), dtype=np.float64)
        
        # Integration between lower and upper bounds of annular rings
        for p in range(self.nkr):
            idk = (self.kh >= self.kr[p]-self.dkr/2) & (self.kh < self.kr[p]+self.dkr/2)
            fpsd[...,p] = self.scaling * np.sum(fhat2[...,idk], axis=-1) 
        return fpsd 

if __name__ == "__main__":

    import os
    import torch
    import matplotlib.pyplot as plt
    
    # Set param
    path = 'outputs'
    filenames = ['det_downsampled','det','LU_enstrophy','Perturbed_points','Perturbed_steps','Without_compensation']
    pltlabels = ['Deterministic HR','Deterministic','LU','Perturbed points','Perturbed steps','Without compensation']
    pltcolors = ['#8033ac','#6588cd','#000000','#c65c8a','#94b927','#00a9b2']
    outpath = 'figures'

    # Init plot of spectrum
    fig, ax = plt.subplots(1,2,sharex=True,constrained_layout=True,figsize=(10,3.5))
    os.makedirs(outpath) if not os.path.isdir(outpath) else None
    
    for m in range(len(filenames)):
        # Init spectrum
        param = torch.load(os.path.join(path,filenames[m],'param.pth'))
        psd = PSD(param)
  
        # Read data
        data = np.load(os.path.join(path,filenames[m],'pq_365.npz'))
        p = data['p'].astype('float64')
        data.close()

        # Compute spectrum
        mke_spec, tke_spec = psd.compute_spectrum(p)

        # Plot spectrum
        ax[0].loglog(psd.kr, mke_spec[0], label=pltlabels[m], c=pltcolors[m])
        ax[1].loglog(psd.kr, tke_spec[0], label=pltlabels[m], c=pltcolors[m])

    for c in range(2):
        ax[c].legend()
        ax[c].grid(which='both', axis='both')
        ax[c].set_xlabel('Isotropic wavenumber')
    ax[0].set(title='Mean kinetic energy spectrum', xlim=(1.5e-6,1e-4), ylim=(1,200), ylabel='Spectral density')
    ax[1].set(title='Turbulent kinetic energy spectrum', ylim=(0.01,130))
    #plt.show()
    filename = os.path.join(outpath,'comp_spec.pdf')
    plt.savefig(filename, dpi=300, bbox_inches='tight', pad_inches=0)
    
