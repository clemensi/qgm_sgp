import numpy as np
import torch
import torch.nn.functional as F
import QGM_det_HR
import os
import IO_QGM
from exploit_results import readFromFiles
from description_experiments import create_param

def fast_integrate_in_time(n_ens:int, n_steps: int, dt: float,
        nx: int, ny: int,
        freq_save:int, a4: float, rand_model: str,
        initial_file: str, output_dir: str):
    torch.backends.cudnn.deterministic = True

    param = create_param(nx, ny, dxy=3840e3/(nx-1), dt=dt,
            n_ens=n_ens, a4=a4, rand_model=rand_model)
    qgm = QGM_det_HR.QGM(param)

    # Starting input/output module and initializing qgm:
    Input_output_QGM = IO_QGM.IO_QGM(output_dir,
            freq_save=freq_save, #freq_log=int(30*24*3600/param['dt']))
            freq_log=int(12*3600/param['dt']),
            freq_plot=0)
    # monkey patching of the function "qgm.compute_dp_modes"
    Input_output_QGM.initialize_pq(qgm, initial_file)
    Input_output_QGM.initialize_IO(param)
    Input_output_QGM.save_plot_log(qgm) # outputs initial state

    # Time-stepping
    for _ in range(n_steps):
        qgm.step()
        Input_output_QGM.save_plot_log(qgm)

def create_initial_ensemble():
    """
        Helper function which takes the 30 first outputs of det_HR
        to create an ensemble of HR solutions.
        before this function, please call QGM_det_HR with parameters:
        n_steps = int(15 *365*24*3600/dt) + 1 #15 year
        freq_save = int(365*12*3600/dt) # 6 months

        This ensemble is then to be used as input by a LU 40km integrator
        during 1 year to adjust the energy of the system to the low resolution
        setting.
    """
    dt=600.
    print("Launching 15 years of High resolution simulation, with output every 6 months")
    fast_integrate_in_time(n_ens=1, n_steps=int(15 *365*24*3600/dt) + 1, dt=dt,
            nx=769, ny=961,
            freq_save=int(365*12*3600/dt), a4=2.0e9,
            initial_file="input_data/lastday_10km.npz",
            output_dir="input_data/spinup1", rand_model="")

    print("Creating an ensemble of high-resolution states...")
    filenames = tuple("input_data/spinup1/pq_" + str(t) + ".npz" for t in range(30))
    all_members, = readFromFiles(filenames, ("p",))
    np.savez("input_data/ensemble_5km", t=0., p=all_members[:,0,...])
    print("Ensemble stored in input_data/ensemble_5km.npz")
    print("deleting folder input_data/spinup1")
    import shutil
    shutil.rmtree("input_data/spinup1")

    print("Integrating in time with the Low-resolution code the previous ensemble...")
    dt=1800.
    fast_integrate_in_time(n_ens=30, n_steps=int(365*24*3600/dt) + 1, dt=dt,
            nx=97, ny=121, freq_save=int(365*24*3600/dt), a4=5e11,
            initial_file="input_data/ensemble_5km.npz",
            output_dir="input_data/spinup2", rand_model="enstrophy_preserve")
    print("Done.")
    print("Result of the 1-year simulation stored into file input_data/initial_ensemble.npz")
    shutil.copy("input_data/spinup2/pq_1.npz", "input_data/initial_ensemble.npz")
    print("deleting folder input_data/spinup2")
    shutil.rmtree("input_data/spinup2")
    
if __name__ == "__main__":
    create_initial_ensemble()
