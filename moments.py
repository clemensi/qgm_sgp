import os
import numpy as np
import torch
import scipy.stats
from description_experiments import Experiment, FOLDERNAMES, EXP_NAMES
import exploit_results
import matplotlib.pyplot as plt

def moment_pointwise(variable: str, moment_function: callable, is_std=False):
    fig, axes = exploit_results.subplot_qualitative_figure()
    experiments = exploit_results.all_experiments
    # Data gathering: getting pressure for every experiment
    params = torch.load(FOLDERNAMES[Experiment.det]+"param.pth")
    extent = [0, params["Lx"], 0, params["Ly"]]
    time_range_mean = range(358,365)
    for ax, experiment in zip(axes, experiments):
        try:
            f = exploit_results.filenames_for_readFromFiles(experiment, time_range_mean)
            p_or_q, = exploit_results.readFromFiles(f, (variable,))
            if variable == "q":
                f0 = 9.37456e-5 # Coriolis param. (s^-1)
                p_or_q *= f0
            image = exploit_results.pressure_or_vorticity_plot(ax,
                    moment_function(p_or_q), experiment, extent, variable, for_std=is_std)
        except FileNotFoundError:
            ax.text(extent[1]/5,extent[-1]/2,"Missing file(s)")
    fig.colorbar(image, ax=axes, orientation='vertical', fraction=.1)

def compute_moments_q(experiment: Experiment, centered_on_jet):
    filenames = exploit_results.filenames_for_readFromFiles(experiment, exploit_results.TIME_RANGE)
    var, = exploit_results.readFromFiles(filenames, ("q",))
    return compute_moments(var, centered_on_jet)

def compute_moments(var: np.ndarray, centered_on_jet: bool=False):
    if centered_on_jet:
        Nx, Ny = var.shape[-2:]
        var = var[..., Nx//5:3*Nx//5, Ny//3:2*Ny//3]
    moments = np.empty((4, var.shape[0], var.shape[1]))
    moments[0] = np.mean(var, axis=(-1,-2,-3))
    moments[1] = np.std(var, axis=(-1,-2,-3))
    for index_time in range(var.shape[0]):
        for index_ensemble in range(var.shape[1]):
            moments[2, index_time, index_ensemble] = \
                    scipy.stats.skew(var[index_time,index_ensemble].flatten())
            moments[3, index_time, index_ensemble] = \
                    scipy.stats.kurtosis(var[index_time,index_ensemble].flatten())
    return moments

def print_moments_sinkloss(distance_to_LU_function, centered_on_jet:bool):
    from geomloss import SamplesLoss
    loss = SamplesLoss(loss="sinkhorn", p=2, blur=.001)
    experiments = (Experiment.det, Experiment.Perturbed_points,
            Experiment.Without_compensation, Experiment.Perturbed_steps)
    distance_to_LU = [distance_to_LU_function(exp, centered_on_jet) for exp in experiments]
    print(r"\begin{tabular}{c|c|c|c|c}")
    print(r" & Deterministic & Perturbed points & Without compensation & Perturbed steps")
    for moment, name in enumerate(("MEAN", "STD", "SKEWNESS", "KURTOSIS")):
        print("\\\\", name)
        for i in range(4):
            print("&", f"{distance_to_LU[i][moment]:.3f}")
    print(r"\end{tabular}")

def get_moments_u(centered_on_jet:bool):
    moments_dir = "tmp_moments" + ("_jet_centered" if centered_on_jet else "")
    os.makedirs(moments_dir) if not os.path.isdir(moments_dir) else None
    filename = os.path.join(moments_dir,"moments_u.npy")
    try:
        all_moments = np.load(filename)
    except:
        print("% Precomputed moments of u not found. Computing moments...")
        experiments = exploit_results.all_experiments
        moments = []
        for experiment in experiments:
            filenames = exploit_results.filenames_for_readFromFiles(experiment, exploit_results.TIME_RANGE)
            params = torch.load(FOLDERNAMES[experiment]+"param.pth")
            try:
                p, dx, dy = exploit_results.readFromFiles(filenames, ("p", "dx", "dy"))
                u, v = exploit_results.compute_velocity_PG(p,
                                         dx + params["Lx"] / (params["nx"]-1),
                                         dy + params["Ly"] / (params["ny"]-1),
                                         params["f0"])
            except KeyError as e : # no dx, dy in files ?
                p, = exploit_results.readFromFiles(filenames, ("p",))
                u, v = exploit_results.compute_velocity(p, params["Lx"] / (params["nx"]-1),
                                              params["Ly"] / (params["ny"]-1), params["f0"])
            moments += [compute_moments(u, centered_on_jet)]
        all_moments = np.array(moments)
        print("% Moments computed.")
        np.save(filename, all_moments)
    return all_moments

def get_moments_q(centered_on_jet:bool):
    moments_dir = "tmp_moments" + ("_jet_centered" if centered_on_jet else "")
    os.makedirs(moments_dir) if not os.path.isdir(moments_dir) else None
    filename = os.path.join(moments_dir,"moments_q.npy")
    try:
        all_moments = np.load(filename)
    except:
        print("% Precomputed moments of q not found. Computing moments...")
        all_moments = []
        for experiment in exploit_results.all_experiments:
            all_moments += [compute_moments_q(experiment, centered_on_jet)]
        all_moments = np.array(all_moments)
        print("% Moments computed.")
        np.save(filename, all_moments)
    return all_moments

def distance_to_LU_moments_u(exp: Experiment, centered_on_jet:bool):
    return compute_moments_sinkloss(get_moments_u(centered_on_jet), exp)

def distance_to_LU_moments_q(exp: Experiment, centered_on_jet: bool):
    return compute_moments_sinkloss(get_moments_q(centered_on_jet), exp)

def compute_moments_sinkloss(all_moments: np.ndarray, experiment: Experiment):
    """
        returns the relative distance to LU for each moment.
    """
    from geomloss import SamplesLoss
    loss = SamplesLoss(loss="sinkhorn", p=2, blur=.001)
    ret = []
    for moment, name in enumerate(("MEAN", "STD", "SKEWNESS", "KURTOSIS")):
        LU_members = torch.from_numpy(all_moments[0][moment])
        reference_distance = loss((LU_members.T)[:15], (LU_members.T)[15:]).item()
        number_of_members = LU_members.shape[0]
        exp_number = exploit_results.all_experiments.index(experiment)
        compared_members = torch.from_numpy(all_moments[exp_number][moment])
        ret += [loss((LU_members.T), (compared_members.T)).item()/reference_distance]
    return ret
