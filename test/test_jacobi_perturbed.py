import torch
import numpy as np
from QGM import jacobi_perturbed

def jacobi_h(f, g):
    """Reference function:
        Arakawa discretisation of Jacobian J(f,g), on a cartesian grid.
       Scalar fields f and g must have the same dimension.
       Grid is regular and dx = dy."""
    dx_f = f[...,2:,:] - f[...,:-2,:]
    dx_g = g[...,2:,:] - g[...,:-2,:]
    dy_f = f[...,2:] - f[...,:-2]
    dy_g = g[...,2:] - g[...,:-2]
    return (
            (   dx_f[...,1:-1] * dy_g[...,1:-1,:] - dx_g[...,1:-1] * dy_f[...,1:-1,:]  ) +
            (   (f[...,2:,1:-1] * dy_g[...,2:,:] - f[...,:-2,1:-1] * dy_g[...,:-2,:]) -
                (f[...,1:-1,2:]  * dx_g[...,2:] - f[...,1:-1,:-2] * dx_g[...,:-2])     ) +
            (   (g[...,1:-1,2:] * dx_f[...,2:] - g[...,1:-1,:-2] * dx_f[...,:-2]) -
                (g[...,2:,1:-1] * dy_f[...,2:,:] - g[...,:-2,1:-1] * dy_f[...,:-2,:])  )
           ) / 12.

def compute_jacobi_direct(f, g, nx, ny, dx, dy):
    """
    interface to compare jacobi_perturbed with the reference on cartesian grid.
    """
    dx_half = (np.ones((1, 1, nx+1, ny)) * dx)
    dy_half = (np.ones((1, 1, nx, ny+1)) * dy)
    return jacobi_perturbed(f, g, dx_half, dy_half) * dx * dy

def test_simple_jacobi():
    nx, ny = 3,3
    dx, dy = 0.1, 0.1
    f = np.ones((1, 1, nx, ny)) * np.linspace(.1,.5, ny)**4 * (np.linspace(.1,3,nx)[...,None])**2
    g = np.ones((1, 1, nx, ny)) * np.linspace(.1,.5, ny)**3 * (np.linspace(.1,3,nx)[...,None])**7
    assert np.linalg.norm(jacobi_h(f, g) - compute_jacobi_direct(f, g, nx, ny, dx, dy))<1e-12

def test_jacobi_cartesian_grid():
    nx, ny = 4,5
    dx, dy = 0.1, 0.35
    f = np.ones((1, 1, nx, ny)) * np.linspace(0.4,1, ny)**4 * (np.linspace(.1,3,nx)[...,None])**2
    g = np.ones((1, 1, nx, ny)) * np.linspace(0.1,1, ny)**3 * (np.linspace(.4,.8,nx)[...,None])**7
    assert np.linalg.norm(jacobi_h(f, g) - compute_jacobi_direct(f, g, nx, ny, dx, dy))<1e-12

if __name__ == "__main__":
    test_simple_jacobi()
    test_jacobi_cartesian_grid()
