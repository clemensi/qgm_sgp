import torch
import numpy as np
from QGM import curl_wind, interp_1d, interp_2d

def reference_curl_wind(tau, dx, dy):
    tau_x = 0.5 * (tau[:-1,:,0] + tau[1:,:,0])
    tau_y = 0.5 * (tau[:,:-1,1] + tau[:,1:,1])
    curl_stagg = (tau_y[1:] - tau_y[:-1]) / dx - (tau_x[:,1:] - tau_x[:,:-1]) / dy
    return  0.25*(curl_stagg[:-1,:-1] + curl_stagg[:-1,1:] + curl_stagg[1:,:-1] + curl_stagg[1:,1:])

def compute_on_cartesian_grid(nx, ny, dx, dy, function, *args):
    """
    interface to compare laplacian functions with the references
    """
    dx_half = (np.ones((1, 1, nx+1, ny)) * dx)
    dy_half = (np.ones((1, 1, nx, ny+1)) * dy)
    return function(*args, dx_half, dy_half)

def test_simple_curl():
    nx, ny = 4, 5
    dx, dy = 0.1, 0.3
    tau = np.ones((nx, ny, 2)) * (np.linspace(.1,.5, ny)[None, :, None])**4 * (np.linspace(.1,3,nx)[:,None, None])**2
    ref = reference_curl_wind(tau, dx, dy)
    curl = compute_on_cartesian_grid(nx, ny, dx, dy, curl_wind, tau)
    assert np.linalg.norm(ref-curl) < 1e-5

def test_simple_interp1d():
    f = torch.tensor((0,1))
    assert abs(.5 - interp_1d(f, 0)[0]) < 1e-5

def test_interp1d_dim2():
    f = torch.tensor(((0,4),(1, 5)))
    assert torch.linalg.norm(torch.tensor(((.5, 4.5),)) - interp_1d(f, dim=0)) < 1e-5
    assert torch.linalg.norm(torch.tensor(((2.,), (3.,))) - interp_1d(f, dim=1)) < 1e-5

def test_interp2d():
    f = torch.tensor(((0,1), (1,2)))
    assert abs(interp_2d(f)[0] - 1) < 1e-5
    assert interp_2d(f).shape == torch.Size([1,1])

if __name__ == "__main__":
    test_interp2d()
    test_interp1d_dim2()
    test_simple_interp1d()
    test_simple_curl()
