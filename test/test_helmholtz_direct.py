import torch
import numpy as np
from scipy.linalg import solve_banded
from QGM import helmholtz_direct_grid_detailed

def compute_helmholtz_direct(nx, ny, dx, dy, f0, lambd, arr_kwargs):
    """
    Basic FD helmholtz operator with structured grid
    notations: p are on the "full" grid points,
        and the distance on the x-axis between p_i and p_{i-1} is dx_half_i
            ("half=-1/2")
        dx_full_i is (dx_half_i + dx_half_{i+1})/2
    """
    x, y = torch.meshgrid(torch.arange(0,nx, **arr_kwargs),
                          torch.arange(0,ny, **arr_kwargs),
                          indexing='ij')
    # dx_half[0,i] = x[0,i] - x_[-1,i] (where -1 is *not* the last element but a virtual one)
    dx_half = (np.ones((1, 1, nx+1, ny)) * dx)
    # dy_half[j,0] = y[j,0] - y[j,-1] (where -1 is *not* the last element but a virtual one)
    dy_half = (np.ones((1, 1, nx, ny+1)) * dy) # dy_half[0,j] = y[0,j] - y[0,-1]
    return helmholtz_direct_grid_detailed(dx_half, dy_half, f0, (lambd,), arr_kwargs)[0][0]


def test_u_simple():
    nx, ny = 3, 3
    dx, dy = 1., 1.
    f0 = 1.
    lambd = 0.
    arr_kwargs = {}
    ab = compute_helmholtz_direct(nx, ny, dx, dy, f0, lambd, arr_kwargs)
    l_and_u = (ab.shape[0]//2, ab.shape[0]//2)
    x, y = np.meshgrid(np.arange(0,nx, **arr_kwargs),
                          np.arange(0,ny, **arr_kwargs),
                          indexing='ij')
    u = x+y
    helmholtz_ret = mult_banded(ab,u.flatten()).reshape((nx,ny))
    ab_times_u = np.array([2,0,-4, 0,0,-4,-4,-4,-10]).reshape((nx,ny)).T
    assert np.linalg.norm(ab_times_u - helmholtz_ret) < 1e-12

def test_u_stretched():
    nx, ny = 3, 3
    dx, dy = 10., 1.
    f0 = 1.
    lambd = 0.
    arr_kwargs = {}
    ab = compute_helmholtz_direct(nx, ny, dx, dy, f0, lambd, arr_kwargs)
    l_and_u = (ab.shape[0]//2, ab.shape[0]//2)
    x, y = np.meshgrid(np.arange(0,nx, **arr_kwargs),
                          np.arange(0,ny, **arr_kwargs),
                          indexing='ij')
    u = x+y
    helmholtz_ret = mult_banded(ab,u.flatten()).reshape((nx,ny))
    ab_times_u = np.array([1.01,0,-1.03, 0,0,-.04,-3.01,-4,-5.05]).reshape((nx,ny))
    assert np.linalg.norm(ab_times_u - helmholtz_ret) < 1e-12

def mult_banded(ab, u):
    """
        Returns "ab * u" where ab is the input of solve_banded
        normally, one should always have the equality
            mult_banded(ab, solve_banded(ab,f)) == f
        we assume there are as much lower and upper diagonals.
        assuming ab has zeros in the unused places ("*" in scipy doc)
    """
    l_or_u = ab.shape[0]//2
    tmp_mul = ab * u[None,:]
    ret = tmp_mul[l_or_u]
    for k in range(1, l_or_u+1):
        ret[:-k] += tmp_mul[l_or_u-k, k:]
        ret[k:] += tmp_mul[l_or_u+k, :-k]
    return ret

if __name__ == "__main__":
    test_u_simple()
    test_u_stretched()
