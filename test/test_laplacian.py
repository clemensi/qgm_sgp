import torch
import numpy as np
from QGM import laplacian_boundaries, laplacian_nobc, laplacian

def reference_laplacian_h_boundaries(f, fc):
    return fc*(torch.cat([f[...,1,1:-1],f[...,-2,1:-1], f[...,1], f[...,-2]], dim=-1) -
               torch.cat([f[...,0,1:-1],f[...,-1,1:-1], f[...,0], f[...,-1]], dim=-1))

def reference_laplacian_h_nobc(f):
    return (f[...,2:,1:-1] + f[...,:-2,1:-1] + f[...,1:-1,2:] + f[...,1:-1,:-2]
            - 4*f[...,1:-1,1:-1])

def reference_laplacian_h(f, fc):
    delta_f = torch.zeros_like(f)
    delta_f[...,1:-1,1:-1] = reference_laplacian_h_nobc(f)
    delta_f_bound = reference_laplacian_h_boundaries(f, fc)
    nx, ny = f.shape[-2:]
    delta_f[...,0,1:-1] = delta_f_bound[...,:ny-2]
    delta_f[...,-1,1:-1] = delta_f_bound[...,ny-2:2*ny-4]
    delta_f[...,0] = delta_f_bound[...,2*ny-4:nx+2*ny-4]
    delta_f[...,-1] = delta_f_bound[...,nx+2*ny-4:2*nx+2*ny-4]
    return delta_f

def compute_on_cartesian_grid(nx, ny, dx, dy, function, *args):
    """
    interface to compare laplacian functions with the references
    """
    dx_half = (torch.ones((1, 1, nx+1, ny)) * dx)
    dy_half = (torch.ones((1, 1, nx, ny+1)) * dy)
    return function(*args, dx_half, dy_half) * dx * dy

def test_simple_laplacian_boundaries():
    nx, ny = 4,5
    dx, dy = 0.1, 0.35
    f = torch.ones((1, 1, nx, ny)) * torch.linspace(.1,.5, ny)**4 * (torch.linspace(.1,3,nx)[...,None])**2
    fc = 14.
    ref = reference_laplacian_h_boundaries(f, fc)
    laplacian_boundaries_ret = compute_on_cartesian_grid(nx, ny, dx, dy, laplacian_boundaries, f, fc)
    assert np.linalg.norm(ref-laplacian_boundaries_ret) < 1e-7

def test_simple_laplacian_nobc():
    nx, ny = 4,5
    dx, dy = 0.1, 0.1 # Note: we cannot compare for dx != dy because reference is too simple
    f = torch.ones((1, 1, nx, ny)) * torch.linspace(.1,.5, ny)**4 * (torch.linspace(.1,3,nx)[...,None])**2
    ref = reference_laplacian_h_nobc(torch.clone(f))
    laplacian_nobc_ret = compute_on_cartesian_grid(nx, ny, dx, dy, laplacian_nobc, f)
    assert np.linalg.norm(ref-laplacian_nobc_ret) < 1e-7

def test_simple_laplacian():
    nx, ny = 4,5
    dx, dy = 0.1, 0.1
    f = torch.ones((1, 1, nx, ny)) * torch.linspace(.1,.5, ny)**4 * (torch.linspace(.1,3,nx)[...,None])**2
    fc = 14.
    ref = reference_laplacian_h(f, fc)
    laplacian_ret = compute_on_cartesian_grid(nx, ny, dx, dy, laplacian, f, fc)
    assert np.linalg.norm(ref-laplacian_ret) < 1e-7

def test_linear_laplacian():
    nx, ny = 4,5
    dx, dy = 0.1, 0.3
    f = torch.ones((1, 1, nx, ny)) * torch.linspace(.1,1., ny) * (torch.linspace(.1,10,nx)[...,None])
    fc = 0.
    laplacian_ret = compute_on_cartesian_grid(nx, ny, dx, dy, laplacian, f, fc)
    print(torch.linalg.norm(laplacian_ret).item())
    assert torch.linalg.norm(laplacian_ret).item() < 1e-5

if __name__ == "__main__":
    test_linear_laplacian()
    test_simple_laplacian_nobc()
    test_simple_laplacian()
    test_simple_laplacian_boundaries()
