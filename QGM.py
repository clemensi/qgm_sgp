"""Pytorch multilayer QG model, Louis Thiry, 2022.
Followed Q-GCM user guide, Hogg et al (2014), http://q-gcm.org/downloads/q-gcm-v1.5.0.pdf.
   - State variables are pressure p and the potential vorticity q.
  - Rectangular domain, mixed slip boundary condition for velocity.
  - Advection of q is carried with the conservative Arakawa-Lamb scheme.
  - (Bi-)Laplacian diffusion discretized with centered finite difference.
  - Idealized double-gyre wind forcing
  - Time integration with SSPRK3.
  - Tested on Intel CPU and NVIDIA GPU with pytorch 1.10 and CUDA 11.2.
Modified by Simon Clement, 2024 to include stochastic grid perturbation.
"""
import numpy as np
import sys
import torch
import torch.nn.functional as F
import matplotlib.pyplot as plt
from scipy.linalg import solve_banded
from concurrent.futures import ThreadPoolExecutor
from description_experiments import EXPERIMENT_BY_NAME

## functions to solve helmholtz equation with homogeneous boundary conditions

def helmholtz_direct_grid_detailed(dx_half, dy_half, f0, lambd, arr_kwargs):
    """
    Basic FD helmholtz operator where grid is specified through arrays of space steps
    notations: p are on the "full" grid points,
        and the distance on the x-axis between p[x,y] and p[x-1,y] is dx_half[x,y]
            ("half=-1/2")
        dx_full[:, :] is (dx_half[1:] + dx_half[:-1])/2
        WARNING shape is (nx, ny) and not (ny, nx) (fortran-contiguous)
        lambd is an array of size nl.
        The function returns one ndarray for each lambd to put in solve_banded.
        The function now returns for each lambd a list of n_ens solve_banded inputs.
    """
    n_ens, nl, nxp1, ny = dx_half.shape
    nx = nxp1 - 1
    assert dy_half.shape[-2:] == (nx, ny+1)
    dx_full = (dx_half[..., 1:, :] + dx_half[..., :-1, :])/2 # dx_full[0, i] = x[1/2,i] - x[-1/2,i]
    dy_full = (dy_half[..., 1:] + dy_half[..., :-1])/2 # dy_full[j,0] = y[j,1/2] - y[j,-1/2]
    # we'll have 3 different helmholtz solvings with 3 different lambd and possibly 3 grids
    # for each ensemble member.
    ret = []
    for i_layer in range(nl):
        ret += [[]]
        for n in range(n_ens):
            lambd_layer = lambd[i_layer]
            # upper/lower diag: 2nd order derivative on x axis
            upper_diag = (1 / dx_half[n, i_layer, 1:, :] / dx_full[n, i_layer]).flatten()
            lower_diag = (1 / dx_half[n, i_layer, :-1, :] / dx_full[n, i_layer]).flatten()
            # far upper/far lower diag: 2nd order derivative on y axis
            far_upper_diag = (1 / dy_half[n, i_layer, :, 1:] / dy_full[n, i_layer]).flatten()
            far_lower_diag = (1 / dy_half[n, i_layer, :, :-1] / dy_full[n, i_layer]).flatten()
            diag = - upper_diag - lower_diag - far_upper_diag - far_lower_diag
            # applying zero pressure boundary condition on left and right:
            upper_diag[ny-1::ny] = 0.
            lower_diag[::ny] = 0.

            diag_helmholtz = diag - lambd[i_layer] * f0**2
            # creating banded matrix for scipy.linalg.solve_banded:
            ret[-1] += [np.vstack([np.concatenate((np.zeros(ny), far_upper_diag[:-ny]))] \
                    + [np.zeros(nx*ny)] * (ny-2) \
                    + [np.concatenate(([0], upper_diag[:-1])),
                            diag_helmholtz,
                            np.concatenate((lower_diag[1:], [0]))] \
                    + [np.zeros(nx*ny)] * (ny-2) \
                    + [np.concatenate((far_lower_diag[ny:], np.zeros(ny)))]) / f0**2]
    return ret

def jacobi_perturbed(f, g, dx_half, dy_half):
    """
    Arakawa discretisation of Jacobian
    (see Kacimi et al, 2013 for notations).
    The conservations properties are lost
    if the grid spacing is not constant.
    """
    double_dx = dx_half[..., 2:-1, :] + dx_half[..., 1:-2,:]
    double_dy = dy_half[..., :, 2:-1] + dy_half[..., :, 1:-2]
    dx_f = (f[...,2:,:] - f[...,:-2,:])/double_dx
    dx_g = (g[...,2:,:] - g[...,:-2,:])/double_dx
    dy_f = (f[...,2:] - f[...,:-2])/double_dy
    dy_g = (g[...,2:] - g[...,:-2])/double_dy
    # J_{++} : most obvious discretization of Jacobian
    # (conserves symmetry)
    J_pp = dx_f[...,1:-1] * dy_g[...,1:-1,:] - dx_g[...,1:-1] * dy_f[...,1:-1,:]
    # J_{+x} : flux form Jacobian
    # (conserves domain integrated enstrophy) 
    J_px = (f[...,2:,1:-1] * dy_g[...,2:,:] - f[...,:-2,1:-1] * dy_g[...,:-2,:]) / double_dx[..., 1:-1] - \
            (f[...,1:-1,2:]  * dx_g[...,2:] - f[...,1:-1,:-2] * dx_g[...,:-2]) / double_dy[..., 1:-1, :]
    # J_{x+} : other flux form Jacobian
    # (conserves integral of energy) 
    J_xp = (g[...,1:-1,2:] * dx_f[...,2:] - g[...,1:-1,:-2] * dx_f[...,:-2]) / double_dy[..., 1:-1, :] - \
           (g[...,2:,1:-1] * dy_f[...,2:,:] - g[...,:-2,1:-1] * dy_f[...,:-2,:]) / double_dx[..., 1:-1]
    return (J_pp + J_px + J_xp) / 3.

def laplacian_boundaries(f, fc, dx_half, dy_half):
    """
        Returns the normal derivative of f on the boundaries,
        divided by the space step in the direction perpendicular to the normal.
    """
    dxy1 = torch.cat([dx_half[...,1,1:-1], dx_half[...,-2, 1:-1],
                       dy_half[..., 1], dy_half[..., -2]], dim=-1)

    dxy2 = torch.cat([dy_half[...,0,2:-1], dy_half[...,-1, 2:-1], # the division by dxy2 is here
                       dx_half[..., 1:, 0], dx_half[...,1:, -1]], dim=-1) # to match dimensions.

    fc_dxy = fc*(torch.cat([f[...,1,1:-1],f[...,-2,1:-1], f[...,1], f[...,-2]], dim=-1) -
               torch.cat([f[...,0,1:-1],f[...,-1,1:-1], f[...,0], f[...,-1]], dim=-1))
    return fc_dxy / dxy1 / dxy2

def laplacian_nobc(f, dx_half, dy_half):
    """
     returns the interior of Lapl(f): to be used with laplacian_boundaries
     (see function laplacian)
    """
    dx_full = (dx_half[..., 1:, :] + dx_half[..., :-1, :])/2 # dx_full[0, i] = x[1/2,i] - x[-1/2,i]
    dy_full = (dy_half[..., 1:] + dy_half[..., :-1])/2 # dy_full[j,0] = y[j,1/2] - y[j,-1/2]
    dxf_p = (f[...,2:,1:-1] - f[...,1:-1,1:-1]) / dx_half[..., 2:-1, 1:-1]
    dxf_m = (f[...,1:-1,1:-1] - f[...,:-2,1:-1]) / dx_half[..., 1:-2, 1:-1]
    dyf_p = (f[...,1:-1,2:]  - f[...,1:-1,1:-1]) / dy_half[..., 1:-1, 2:-1]
    dyf_m =  (f[...,1:-1,1:-1] - f[...,1:-1,:-2]) / dy_half[..., 1:-1, 1:-2]
    dxxf = (dxf_p - dxf_m) / dx_full[..., 1:-1,1:-1]
    dyyf = (dyf_p - dyf_m) / dy_full[..., 1:-1,1:-1]
    return dxxf + dyyf

def laplacian(f, fc, dx_half, dy_half):
    """
        returns Laplacian of f
        f: function whose laplacian is to be evaluated
        fc: value on the boundary
        dx_half, dy_half: grid
    """
    delta_f = torch.zeros_like(f)
    delta_f[...,1:-1,1:-1] = laplacian_nobc(f, dx_half, dy_half)
    delta_f_bound = laplacian_boundaries(f, fc, dx_half, dy_half)
    nx, ny = f.shape[-2:]
    delta_f[...,0,1:-1] = delta_f_bound[...,:ny-2]
    delta_f[...,-1,1:-1] = delta_f_bound[...,ny-2:2*ny-4]
    delta_f[...,0] = delta_f_bound[...,2*ny-4:nx+2*ny-4]
    delta_f[...,-1] = delta_f_bound[...,nx+2*ny-4:2*nx+2*ny-4]
    return delta_f

def curl_wind(tau, dx_half, dy_half):
    """
        computes the curl of tau
        tau is located on the 'full' grid points. (p-points)
        the wind is defined at the surface (layer 0)
    """
    dx_half_avg_along_y = .5 * (dx_half[..., 0, 1:-1, 1:] + dx_half[..., 0, 1:-1, :-1])
    dy_half_avg_along_x = .5 * (dy_half[..., 0, 1:, 1:-1] + dy_half[..., 0, :-1, 1:-1])
    tau_x = 0.5 * (tau[:-1,:,0] + tau[1:,:,0]) # shape : (nx-1, ny)
    tau_y = 0.5 * (tau[:,:-1,1] + tau[:,1:,1]) # shape : (nx, ny-1)
    curl_stagg = (tau_y[1:] - tau_y[:-1]) / dx_half_avg_along_y - \
            (tau_x[:,1:] - tau_x[:,:-1]) / dy_half_avg_along_x
    return  0.25*(curl_stagg[..., :-1,:-1] + \
            curl_stagg[..., :-1,1:] + \
            curl_stagg[..., 1:,:-1] + \
            curl_stagg[..., 1:,1:])

def interp_1d(f, dim):
    """
        This function calculates the center of a cell in one dimension: it does not need the grid
    """
    n = f.shape[dim]
    return (f.narrow(dim, 0, n-1) + f.narrow(dim, 1, n-1))/2

def interp_2d(f):
    """
        This function calculates the value of f in the center of a cell: it does not need the grid
    """
    return (f[...,:-1,:-1] + f[...,:-1,1:] + f[...,1:,:-1] + f[...,1:,1:])/4

class QGM:
    """Implementation of multilayer quasi-geostrophic model
    in variables pressure p and potential vorticity q.
    """

    def __init__(self, param):
        self.nx = param['nx'] # number of p-points in x direction
        self.Lx = param['Lx'] # Length in x direction (m)
        self.ny = param['ny'] # number of p-points in y direction
        self.Ly = param['Ly'] # Length in y direction (m)
        self.nl = param['nl'] # number of layers
        self.H = param['H'] # layer thickness (m)
        self.g_prime = param['g_prime'] # reduced gravities (m/s^2)
        self.f0 = param['f0'] # Coriolis param. (s^-1)
        self.a2 = param['a2'] # laplacian diffusion coef (m^2/s)
        self.a4 = param['a4'] # bi-harmonic visc. coef. (m^4/s)
        self.beta = param['beta'] # Coriolis gradient (m^-1/s)
        self.delta_ek = param['delta_ek']# bottom Ekman layer thickness (m)
        self.dt = param['dt'] # timestep (s)
        self.alpha_bc = param['alpha_bc'] # boundary condition coef. (non-dim.)
        self.n_ens = param['n_ens'] # ensemble size
        self.zfbc = self.alpha_bc / (1. + 0.5*self.alpha_bc)
        self.device = param['device'] # 'cuda' or 'cpu'
        self.compensation_PG = param['compensation_PG']
        self.special_cumulative_sum = param['special_cumulative_sum']
        self.arr_kwargs = {'dtype':torch.float64, 'device':self.device}
        # 'filt_width': filter width to build local fluctuations
        # 'wave_level': wavelet decomposition levels
        # 'tau0': wind stress magnitude (m/s^2)

        # grid
        self.x, self.y = torch.meshgrid(torch.linspace(0, self.Lx, self.nx, **self.arr_kwargs),
                                        torch.linspace(0, self.Ly, self.ny, **self.arr_kwargs),
                                        indexing='ij')
        self.alpha_PG = param['alpha_PG'] # minimum (with eof noise): ~ 1e-5 ?
        self.y0 = 0.5 * self.Ly
        # Approximative space steps:
        self.dx_ref = self.Lx / (self.nx-1)
        self.dy_ref = self.Ly / (self.ny-1)
        assert self.dx_ref == self.dy_ref, f'dx {self.dx_ref} != dy {dy_ref}, must be equal'
        #initialization of grid:
        # dx_half[0,i] = x[0,i] - x_[-1,i] (where -1 is *not* the last element but a virtual one)
        self.dx_half = (torch.ones((self.n_ens, self.nl, self.nx+1, self.ny)) * self.dx_ref)
        # dy_half[j,0] = y[j,0] - y[-1,j] (where -1 is *not* the last element but a virtual one)
        self.dy_half = (torch.ones((self.n_ens, self.nl, self.nx, self.ny+1)) * self.dy_ref)
        self.perturbation_dx = torch.zeros_like(self.dx_half)
        self.perturbation_dy = torch.zeros_like(self.dy_half)
        self.perturbation_dx_old = torch.zeros_like(self.dx_half)
        self.perturbation_dy_old = torch.zeros_like(self.dy_half)

        self.diff_coef = self.a2 / self.f0**2
        self.hyperdiff_coef = self.a4 / self.f0**2
        self.bottom_friction_coef = self.delta_ek / \
                (2*np.abs(self.f0)*(-self.H[-1]))

        tau = torch.zeros((self.nx, self.ny, 2), **self.arr_kwargs)
        tau[:,:,0] = - param['tau0'] * torch.cos(2*torch.pi*(torch.arange(self.ny, **self.arr_kwargs)+0.5)/self.ny).reshape((1, self.ny))
        self.wind_forcing = curl_wind(tau, self.dx_half, self.dy_half) \
                / (self.f0 * self.H[0])

        # init matrices
        self.compute_A_matrix()
        self.compute_layer_to_mode_matrices()

        # initialize pressure p and potential voritcity q
        self.p_shape = (self.n_ens, self.nl, self.nx, self.ny)
        self.p = torch.zeros(self.p_shape, **self.arr_kwargs)
        self.compute_q_over_f0_from_p()

        # Stochastic param.
        self.rand_model = param['rand_model']
        if self.rand_model:
            self.base_noise = param['base_noise']
            if self.base_noise == 'dwt':
                self.filt_width = param['filt_width']
                self.wave_level = param['wave_level']
                # Convolution kernel
                # Box filter for high-pass filtering
                self.filt = torch.ones((1,1,self.filt_width,self.filt_width), **self.arr_kwargs) / self.filt_width**2
                n = self.filt_width // 2
                self.filt_pad_size = (n,n,n,n)
                # Init. wavelet transform and inverse
                torch.set_default_dtype(torch.float64)
                from pytorch_wavelets import DWTForward, DWTInverse 
                self.dwt2 = DWTForward(J=self.wave_level, mode='symmetric', wave='db5').to(self.device) 
                self.idwt2 = DWTInverse(mode='symmetric', wave='db5').to(self.device)
            elif self.base_noise == 'eof':
                data = np.load(param['file_modes'])
                self.u_modes = torch.from_numpy(data['u']).type(torch.float64).to(self.device)
                self.v_modes = torch.from_numpy(data['v']).type(torch.float64).to(self.device)
                data.close()

        self.mean_drift = param['mean_drift']
        if self.mean_drift: 
            data = np.load(self.mean_drift)
            self.u_drift = torch.from_numpy(data['u']).type(torch.float64).to(self.device).unsqueeze(0)
            self.v_drift = torch.from_numpy(data['v']).type(torch.float64).to(self.device).unsqueeze(0)
            data.close()

        # precompile torch functions
        self.zfbc = torch.tensor(self.zfbc, **self.arr_kwargs) # convert to Tensor for tracing
        self.jacobi_perturbed = torch.jit.trace(jacobi_perturbed, (self.q_over_f0, self.p, self.dx_half, self.dy_half))
        self.laplacian = torch.jit.trace(laplacian,
                (self.p, self.zfbc, self.dx_half, self.dy_half))
        self.laplacian_boundaries = torch.jit.trace(laplacian_boundaries, (self.p, self.zfbc, self.dx_half, self.dy_half))
        self.laplacian_nobc = torch.jit.trace(laplacian_nobc,
                (self.p, self.dx_half, self.dy_half))
        # self.laplacian_h_nobc = torch.jit.trace(laplacian_h_nobc, (self.p,))
        interp_y = lambda f: interp_1d(f, dim=-1)
        interp_x = lambda f: interp_1d(f, dim=-2)
        self.interp_x = torch.jit.trace(interp_x, (self.p,))
        self.interp_y = torch.jit.trace(interp_y, (self.p,))
        self.interp_xy = torch.jit.trace(interp_2d, (self.p,))
        
        if self.rand_model:
            u, v = self.compute_velocity(self.p)
            self.rand_adv_tracer = torch.jit.trace(self.rand_adv_tracer, (self.q_over_f0, u, v)) 
        if self.rand_model in ('energy_preserve', "grid_perturbation_steps", "grid_perturbation_points"):
            betay = self.beta * (self.y.unsqueeze(0).unsqueeze(0) - self.y0)
            self.betay_on_u = self.interp_y(betay[...,1:-1,:]) # interoplated on u-points
            self.betay_on_v = self.interp_x(betay[...,1:-1]) # on v-points


    def compute_A_matrix(self):
        self.A = torch.zeros((self.nl,self.nl), **self.arr_kwargs)
        self.A[0,0] = 1./(self.H[0]*self.g_prime[0])
        self.A[0,1] = -1./(self.H[0]*self.g_prime[0])
        for i in range(1, self.nl-1):
            self.A[i,i-1] = -1./(self.H[i]*self.g_prime[i-1])
            self.A[i,i] = 1./self.H[i]*(1/self.g_prime[i] + 1/self.g_prime[i-1])
            self.A[i,i+1] = -1./(self.H[i]*self.g_prime[i])
        self.A[-1,-1] = 1./(self.H[self.nl-1]*self.g_prime[self.nl-2])
        self.A[-1,-2] = -1./(self.H[self.nl-1]*self.g_prime[self.nl-2])


    def compute_layer_to_mode_matrices(self):
        """Matrices to change from layers to modes."""
        lambd_r, R = torch.linalg.eig(self.A)
        lambd_l, L = torch.linalg.eig(self.A.T)
        self.lambd = lambd_r.real
        R, L = R.real, L.real
        self.Cl2m = torch.diag(1./torch.diag(L.T @ R)) @ L.T
        self.Cm2l = R

    def compute_alpha_matrix_online(self, homogeneous_sol):
        M = (self.Cm2l[1:] - self.Cm2l[:-1])[None, :self.nl-1,:self.nl-1] * homogeneous_sol.mean((-1,-2)).reshape((self.n_ens, 1, self.nl-1))
        alpha_matrix = -torch.linalg.solve(M, self.Cm2l[1:,:-1] - self.Cm2l[:-1,:-1])
        return alpha_matrix

    def compute_q_over_f0_from_p(self):
        Ap = torch.einsum('kl,...lij->...kij', self.A, self.p)
        self.q_over_f0 = laplacian(self.p, self.zfbc, self.dx_half, self.dy_half) \
                / self.f0**2 - Ap + (self.beta / self.f0) * (self.y - self.y0)


    def compute_velocity(self, psi):
        """Compute geostrophic velocities (u,v) from streamfunction psi."""
        return -torch.diff(psi, dim=-1) / self.dy_half[..., 1:-1], torch.diff(psi, dim=-2) / self.dx_half[..., 1:-1, :]

    def advection_rhs(self):
        """Advection diffusion RHS for vorticity, only inside domain"""
        padv = self.p.clone() #if self.p_drift is None else self.p - self.p_drift
        rhs = self.jacobi_perturbed(self.q_over_f0, padv, self.dx_half, self.dy_half) / self.f0

        delta2_p = self.laplacian(self.p, self.zfbc, self.dx_half, self.dy_half)
        # I just multiplied delta2_p by 1/dxy
        if self.a2 != 0.:
            rhs += self.diff_coef * self.laplacian_nobc(delta2_p, self.dx_half, self.dy_half)
        if self.a4 != 0.:
            rhs -= self.hyperdiff_coef * self.laplacian_nobc(self.laplacian(\
                    delta2_p, self.zfbc, self.dx_half, self.dy_half), self.dx_half, self.dy_half)

        rhs[...,0,:,:] += self.wind_forcing
        rhs[...,-1:,:,:] += self.bottom_friction_coef * \
                self.laplacian_nobc(self.p[...,-1:,:,:], self.dx_half[...,-1:,:,:], self.dy_half[...,-1:,:,:])
        
        # Stochastic transport noise based on diff. conservation laws
        if self.rand_model == 'energy_preserve':
            dx_full = 0.5 * (self.dx_half[..., 2:-1, 1:-1] + self.dx_half[..., 1:-2, 1:-1])
            dy_full = 0.5 * (self.dy_half[..., 1:-1, 2:-1] + self.dy_half[..., 1:-1, 1:-2])
            # Advection of momentum (then taking curl)
            du, dv = self.transport_by_noise(self.u_noise, self.v_noise) # dv are located at center of horizontal edges
            # and du are located at center of vertical edges
            rhs += torch.diff(dv, dim=-2)/(dx_full*self.f0) - \
                    torch.diff(du, dim=-1)/(dy_full*self.f0)
            # Advection of tracer (stratification)
            rhs += self.rand_adv_tracer(-torch.einsum('kl,...lij->...kij', self.A, self.p),
                    self.u_noise, self.v_noise)
        if self.rand_model == 'enstrophy_preserve':
            # Advection of PV
            rhs += self.rand_adv_tracer(self.q_over_f0, self.u_noise, self.v_noise)
        if self.rand_model in ('grid_perturbation_steps', 'grid_perturbation_points'):
            u_noise, v_noise = self.rand_adv_compensation() # equivalent noise of the compensation
            if self.compensation_PG == "q":
                # Advection of PV
                rhs += self.rand_adv_tracer(self.q_over_f0, u_noise, v_noise)
            else:
                #advection of momentum (then taking curl)
                dx_full = 0.5 * (self.dx_half[..., 2:-1, 1:-1] + self.dx_half[..., 1:-2, 1:-1])
                dy_full = 0.5 * (self.dy_half[..., 1:-1, 2:-1] + self.dy_half[..., 1:-1, 1:-2])
                du, dv = self.transport_by_noise(u_noise, v_noise)
                rhs += torch.diff(dv, dim=-2)/(dx_full*self.f0) - \
                        torch.diff(du, dim=-1)/(dy_full*self.f0)
                # Advection of tracer (stratification)
                rhs += self.rand_adv_tracer(-torch.einsum('kl,...lij->...kij', self.A, self.p),
                        self.u_noise, self.v_noise)
        return rhs


    def compute_dp_modes(self, dq_over_f0):
        rhs_helmholtz = torch.einsum('ml,...lij->...mij', self.Cl2m, dq_over_f0[...,1:-1,1:-1])

        # constant_field satisfies Laplace equation (see Cal{L} in eq (8.23) of the documentation)
        # constant_field = torch.ones((self.nl-1, self.nx, self.ny), **self.arr_kwargs) / (self.nx * self.ny)
        helmholtz_direct_l1, helmholtz_direct_l2, laplace_direct = helmholtz_direct_grid_detailed(self.dx_half, self.dy_half,
                self.f0, self.lambd, self.arr_kwargs)

        l_or_u = helmholtz_direct_l1[0].shape[0] // 2

        homogeneous_sol = torch.empty((self.n_ens, self.nl-1, self.nx, self.ny), **self.arr_kwargs)
        dp_modes = torch.empty((self.n_ens, self.nl, self.nx, self.ny), **self.arr_kwargs)
        rhs_helmholtz = F.pad(rhs_helmholtz, (1,1,1,1)).reshape(self.n_ens, self.nl, self.nx*self.ny)

        with ThreadPoolExecutor() as executor:
            # we now compute the homogeneous Helmholtz equation's solution p = Cal{L}+s*lambda
            # s is zero on the boundaries and solves helmholtz equation inside domain
            future_s0 = [executor.submit(solve_banded,
                                        (l_or_u, l_or_u), helmholtz_direct_l1[i],
                                        np.ones(self.nx*self.ny)/ (self.nx * self.ny),
                                         overwrite_b=True) \
                                                for i in range(self.n_ens)]
            future_s1 = [executor.submit(solve_banded,
                                        (l_or_u, l_or_u), helmholtz_direct_l2[i],
                                        np.ones(self.nx*self.ny)/ (self.nx * self.ny),
                                         overwrite_b=True) \
                                                for i in range(self.n_ens)]
            # Computing dp_modes without spectral method;
            future_dpm0 = [executor.submit(solve_banded,
                                           (l_or_u, l_or_u), helmholtz_direct_l1[i],
                                        torch.squeeze(rhs_helmholtz[i,0])) \
                                                for i in range(self.n_ens)]
            future_dpm1 = [executor.submit(solve_banded,
                                           (l_or_u, l_or_u), helmholtz_direct_l2[i],
                                        torch.squeeze(rhs_helmholtz[i,1])) \
                                                for i in range(self.n_ens)]
            future_dpm2 = [executor.submit(solve_banded,
                                           (l_or_u, l_or_u), laplace_direct[i],
                                        torch.squeeze(rhs_helmholtz[i,2])) \
                                                for i in range(self.n_ens)]
            for i in range(self.n_ens):
                dp_modes[i, 0] = torch.from_numpy(future_dpm0[i].result()).reshape(self.nx, self.ny)
                dp_modes[i, 1] = torch.from_numpy(future_dpm1[i].result()).reshape(self.nx, self.ny)
                dp_modes[i, 2] = torch.from_numpy(future_dpm2[i].result()).reshape(self.nx, self.ny)
                homogeneous_sol[i,0] = torch.ones((self.nx,self.ny), **self.arr_kwargs)/ (self.nx * self.ny) + self.lambd[0] * future_s0[i].result().reshape(self.nx, self.ny)
                homogeneous_sol[i,1] = torch.ones((self.nx,self.ny), **self.arr_kwargs)/ (self.nx * self.ny) + self.lambd[1] * future_s1[i].result().reshape(self.nx, self.ny)

        # Ensure mass conservation (not sure if useful: do we even have the mass conservation
        # despite changing the helmholtz solving method ?)
        alpha_matrix = self.compute_alpha_matrix_online(homogeneous_sol)
        dalpha = (alpha_matrix @ dp_modes[...,:-1,:,:].mean((-2,-1)).unsqueeze(-1)).unsqueeze(-1)
        dp_modes[...,:-1,:,:] += dalpha * homogeneous_sol
        return dp_modes

    def compute_time_derivatives(self):
        # advect vorticity inside of the domain
        dq_over_f0 = F.pad(self.advection_rhs(), (1,1,1,1))

        dp_modes = self.compute_dp_modes(dq_over_f0)
        dp = torch.einsum('lm,...mij->...lij', self.Cm2l, dp_modes)

        # update voriticity on the boundaries
        dp_bound = torch.cat([dp[...,0,1:-1], dp[...,-1,1:-1], dp[...,:,0], dp[...,:,-1]], dim=-1)
        delta_p_bound = self.laplacian_boundaries(dp, self.zfbc, self.dx_half, self.dy_half) / self.f0**2
        dq_over_f0_bound = delta_p_bound - torch.einsum('kl,...lj->...kj', self.A, dp_bound)
        dq_over_f0[...,0,1:-1] = dq_over_f0_bound[...,:self.ny-2]
        dq_over_f0[...,-1,1:-1] = dq_over_f0_bound[...,self.ny-2:2*self.ny-4]
        dq_over_f0[...,0] = dq_over_f0_bound[...,2*self.ny-4:self.nx+2*self.ny-4]
        dq_over_f0[...,-1] = dq_over_f0_bound[...,self.nx+2*self.ny-4:2*self.nx+2*self.ny-4]

        return dq_over_f0, dp

    def step(self):
        """ Time itegration with SSPRK3 scheme."""
        self.noise_generator() if self.rand_model else None
        
        # Stochastic transport noise based on diff. conservation laws
        if self.rand_model in ('grid_perturbation_steps', 'grid_perturbation_points'):
            dx_noise, dy_noise = self.u_noise*self.dt, self.v_noise*self.dt
            damper = np.exp(-self.alpha_PG * self.dt)
            self.perturbation_dx_old[:] = self.perturbation_dx
            self.perturbation_dy_old[:] = self.perturbation_dy
            self.perturbation_dx[...,1:,1:] = self.perturbation_dx[...,1:,1:] * damper + dx_noise
            self.perturbation_dy[...,1:, 1:] = self.perturbation_dy[...,1:, 1:] * damper + dy_noise
            # Remark that there are three grids for three layers : there is hence a problem (?)
            # of vertical alignment
            # the perturbation is here on the spacing between the points :
            if self.rand_model == 'grid_perturbation_points':
                self.dx_half[..., 1:, :] = torch.diff(self.perturbation_dx, axis=-2) + self.dx_ref
                self.dy_half[..., :, 1:] = torch.diff(self.perturbation_dy, axis=-1) + self.dy_ref
            else:
                self.dx_half = self.perturbation_dx + self.dx_ref
                if self.special_cumulative_sum:
                    diff_dy = torch.diff(self.perturbation_dy, axis=-1)
                    Ny = diff_dy.shape[-1] // 2
                    flipped_first_half_y = torch.flip(diff_dy[...,:Ny], dims=(-1,))

                    self.dy_half[..., :, 1:] = torch.cat( \
                            (torch.flip(torch.cumsum(flipped_first_half_y, dim=-1), dims=(-1,)),
                            torch.cumsum(diff_dy[..., Ny:], dim=-1)), dim=-1) + self.dy_ref
                else:
                    self.dy_half = self.perturbation_dy + self.dy_ref

        dt0_q_over_f0, dt0_p = self.compute_time_derivatives()
        self.q_over_f0 += self.dt * dt0_q_over_f0
        self.p += self.dt * dt0_p

        dt1_q_over_f0, dt1_p = self.compute_time_derivatives()
        self.q_over_f0 += (self.dt/4) * (dt1_q_over_f0 - 3*dt0_q_over_f0)
        self.p += (self.dt/4) * (dt1_p - 3*dt0_p)

        dt2_q_over_f0, dt2_p = self.compute_time_derivatives()
        self.q_over_f0 += (self.dt/12) * (8*dt2_q_over_f0 - dt1_q_over_f0 - dt0_q_over_f0)
        self.p += (self.dt/12) * (8*dt2_p - dt1_p - dt0_p)
        

    def noise_generator(self):
        """Generate noise based on pertubation of wavelet coef. of local fluctuations."""
        # Wavelet-based noise
        if self.base_noise == 'dwt':
            # Local fluct. of streamfunction
            psi = self.p / self.f0
            psi_ = F.pad(psi, self.filt_pad_size, mode='replicate')
            psi -= F.conv2d(psi_.reshape((-1,)+psi_.shape[-2:]).unsqueeze(1), self.filt).squeeze(1).reshape(self.p_shape)
            # Randomize wavelet decomp. of psi
            psi_low, psi_high = self.dwt2(psi[...,:-1,:-1])
            psi_low *= torch.randn((self.n_ens,1,)+psi_low.shape[-2:], **self.arr_kwargs)
            for l in range(self.wave_level):
                psi_high[l] *= torch.randn((self.n_ens,1,3,)+psi_high[l].shape[-2:], **self.arr_kwargs) 
            # Build random psi then derive nondivergent velocity noise
            psi_noise = F.pad(self.idwt2((psi_low, psi_high))[...,1:,1:], (1,1,1,1))
            self.u_noise, self.v_noise = self.compute_velocity(psi_noise)  

        # EOF-based noise
        if self.base_noise == 'eof':
            rn = torch.randn((self.n_ens, self.u_modes.shape[0]), **self.arr_kwargs)
            self.u_noise = torch.einsum('rm,mkij->rkij', rn, self.u_modes)
            self.v_noise = torch.einsum('rm,mkij->rkij', rn, self.v_modes)
        
        # Include Girsanov drift
        if self.mean_drift:
            self.u_noise -= self.u_drift
            self.v_noise -= self.v_drift

    def rand_adv_tracer(self, q, u, v):
        """Random advection of tracer q using centered flux scheme."""
        dx_full = 0.5 * (self.dx_half[..., 2:-1, 1:-1] + self.dx_half[..., 1:-2, 1:-1])
        dy_full = 0.5 * (self.dy_half[..., 1:-1, 2:-1] + self.dy_half[..., 1:-1, 1:-2])
        fx = self.interp_xy(u) * self.interp_x(q[...,1:-1]) # colocated with v
        fy = self.interp_xy(v) * self.interp_y(q[...,1:-1,:]) # colocated with u
        return - torch.diff(fx, dim=-2)/dx_full - torch.diff(fy, dim=-1)/dy_full

    def rand_adv_compensation(self):
        # the compensation is a fraction of the perturbation
        if self.compensation_PG == "continuous":
            fraction_perturbation_x = self.alpha_PG * self.perturbation_dx[..., 1:, 1:]
            fraction_perturbation_y = self.alpha_PG * self.perturbation_dy[..., 1:, 1:]
        elif self.compensation_PG in ("discrete", "q"):
            fraction_perturbation_x = (1 - np.exp(-self.alpha_PG*self.dt)) * self.perturbation_dx_old[..., 1:, 1:]
            fraction_perturbation_y = (1 - np.exp(-self.alpha_PG*self.dt)) * self.perturbation_dy_old[..., 1:, 1:]
        elif self.compensation_PG == "none":
            fraction_perturbation_x = torch.zeros_like(self.perturbation_dx[...,1:,1:])
            fraction_perturbation_y = torch.zeros_like(self.perturbation_dy[...,1:,1:])
        else:
            raise
        # computing the associated velocity : note that there is here a small shift :
        # the perturbation isn't collocated with u.
        u_noise = fraction_perturbation_x / self.dt
        v_noise = fraction_perturbation_y / self.dt
        return u_noise, v_noise

    def transport_by_noise(self, u_noise, v_noise):
        """Random advection of momentum using centered flux form."""
        u, v = self.compute_velocity(self.p/self.f0)
        unoi_on_v, vnoi_on_u = self.interp_xy(u_noise), self.interp_xy(v_noise)

        # Advection of u
        dx_full = 0.25 * (self.dx_half[..., 2:-1, 1:] + self.dx_half[..., 1:-2, 1:] + self.dx_half[..., 2:-1, :-1] + self.dx_half[..., 1:-2, :-1])
        dy_full = 0.25 * (self.dy_half[..., 1:, 2:-1] + self.dy_half[..., 1:, 1:-2] + self.dy_half[..., :-1, 2:-1] + self.dy_half[..., :-1, 1:-2])
        fx = self.interp_x(u*u_noise) # fx is in the center of the squares
        fy = self.interp_x(v_noise) * F.pad(self.interp_y(u[...,1:-1,:]), (1,1))
        # while fy co-located with p[...,1:-1,:]
        du = - torch.diff(fx, dim=-2)/dx_full - torch.diff(fy, dim=-1)/self.dy_half[..., 1:-1, 1:-1]

        # Advection of v
        fy = self.interp_y(v*v_noise) # now fy is located in the center of the squares
        fx = self.interp_y(u_noise) * F.pad(self.interp_x(v[...,1:-1]), (0,0,1,1))
        # and fx is co-located with p [...,:,1:-1]
        dv = - torch.diff(fx, dim=-2)/self.dx_half[...,1:-1, 1:-1] - torch.diff(fy, dim=-1)/dy_full
        # Add beta effects
        return du + self.betay_on_u * vnoi_on_u, dv - self.betay_on_v * unoi_on_v

if __name__ == "__main__":
    print("this file is not the entry point of the code.")
    print("Usage: python3 main.py EXPERIMENT")
    print("EXPERIMENT can be " + str(list(EXPERIMENT_BY_NAME.keys())))
    exit()
