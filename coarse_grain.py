import torch
import numpy as np
import os
from QGM_det_HR import QGM
from comp_spec import PSD
import torch.nn.functional as F

# Set param
path_HR = 'outputs/det_10km'
path_LR = 'outputs/det'
path_out = 'outputs/det_downsampled'

# Init HR and LR models
param = torch.load(os.path.join(path_HR,'param.pth'))
qgm_HR = QGM(param)
psd_HR = PSD(param)

param = torch.load(os.path.join(path_LR,'param.pth'))
qgm_LR = QGM(param)
psd_LR = PSD(param)

# Define Gaussian filter
d = (qgm_HR.nx-1) // (qgm_LR.nx-1)
s = d/2 # standard deviation
hw = int(3*s) # half of kernel width
x = torch.linspace(-hw, hw, 2*hw+1, **qgm_LR.arr_kwargs)
x, y = torch.meshgrid(x, x, indexing='ij')
filt = torch.exp(-(x**2 + y**2) / (2*s**2)) / (2*torch.pi*s**2)
filt /= filt.sum()
filt.unsqueeze_(0).unsqueeze_(0)

# Get list of data
pq_files = [f for f in os.listdir(path_HR) if f.startswith("pq_") and f.endswith(".npz")]
os.makedirs(path_out) if not os.path.isdir(path_out) else None
filename = os.path.join(path_out, 'param.pth')
torch.save(param, filename)

for f in pq_files:
    print(f'Coarse graining file: {f}')

    # Read HR data
    data = np.load(os.path.join(path_HR,f))
    qgm_HR.p = torch.from_numpy(data['p']).type(torch.float64).to(qgm_HR.device)
    t = data['t']
    data.close()

    # Downsampling
    p = F.pad(qgm_HR.p, (hw,hw,hw,hw), mode='replicate')
    p = F.conv2d(p.reshape((-1,)+p.shape[-2:]).unsqueeze(1), filt).squeeze(1).reshape(qgm_HR.p_shape)[...,::d,::d]
    qgm_LR.p[...,1:-1,1:-1] = p[...,1:-1,1:-1] # no boundaries

    # Ensure mass conservation
    p_modes = F.pad(torch.einsum('ml,...lij->...mij', qgm_LR.Cl2m, qgm_LR.p[...,1:-1,1:-1]), (1,1,1,1))
    alpha =  (qgm_LR.alpha_matrix @ p_modes[...,:-1,:,:].mean((-2,-1)).unsqueeze(-1)).unsqueeze(-1)
    p_modes[...,:-1,:,:] += alpha * qgm_LR.homogeneous_sol
    qgm_LR.p = torch.einsum('lm,...mij->...lij', qgm_LR.Cm2l, p_modes)
    qgm_LR.compute_q_over_f0_from_p()

    # Save LR data
    filename = os.path.join(path_out,f) 
    np.savez(filename, t=t, 
             p=qgm_LR.p.cpu().numpy().astype('float32'), 
             q=qgm_LR.q_over_f0.cpu().numpy().astype('float32'))

# Plot data
import matplotlib.pyplot as plt
fig, ax = plt.subplots(1,2,constrained_layout=True)
plt_kwargs = {'origin':'lower', 'cmap':'RdBu_r', 'vmin':-3.5, 'vmax':3.5}
ax[0].imshow(qgm_HR.p.cpu().numpy()[0,0].T, **plt_kwargs)
ax[1].imshow(qgm_LR.p.cpu().numpy()[0,0].T, **plt_kwargs)
ax[0].set_title('HR')
ax[1].set_title('LR')
plt.show()

# Check spectrum
mkes_HR, tkes_HR = psd_HR.compute_spectrum(qgm_HR.p)
mkes_LR, tkes_LR = psd_LR.compute_spectrum(qgm_LR.p)
fig, ax = plt.subplots(1,2,sharex=True,constrained_layout=True)
ax[0].loglog(psd_HR.kr, mkes_HR[0], label='HR')
ax[0].loglog(psd_LR.kr, mkes_LR[0], label='LR')
ax[1].loglog(psd_HR.kr, tkes_HR[0], label='HR')
ax[1].loglog(psd_LR.kr, tkes_LR[0], label='LR')
for c in range(2):
    ax[c].legend()
    ax[c].grid(which='both', axis='both')
    ax[c].set_xlabel('Isotropic wavenumber')
ax[0].set(title='Mean kinetic energy spectrum', xlim=(1.5e-6,4e-4), ylim=(0.2,200), ylabel='Spectral density')
ax[1].set(title='Turbulent kinetic energy spectrum', ylim=(0.005,130))
plt.show()
